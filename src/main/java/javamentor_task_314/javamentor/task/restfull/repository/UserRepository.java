package javamentor_task_314.javamentor.task.restfull.repository;

import javamentor_task_314.javamentor.task.restfull.model.User;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Repository
public class UserRepository {

    static final String URL_USERS = "http://91.241.64.178:7081/api/users";
    static final String URL_USERS_DELETE = "http://91.241.64.178:7081/api/users/{id}";
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    List<String> cookie;

    public void getUsers() {

        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.exchange(URL_USERS,
                HttpMethod.GET, entity, String.class);

        cookie = response.getHeaders().get("Set-Cookie");

        /*-------------------POST-------------------*/
        if (cookie != null) {
            headers.set("Cookie", String.join(";", cookie));
        }

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        User newUser = new User(3L, "James", "Brown", (byte) 50);
        HttpEntity<User> requestBody = new HttpEntity<>(newUser, headers);

        ResponseEntity<String> responseEntityPost = restTemplate.postForEntity(URL_USERS, requestBody, String.class);

        /*---------------------PUT-------------------*/
        Map<String, Integer> params = new HashMap<String, Integer>();
        params.put("id", 3);
        User user3 = new User(3L, "Thomas", "Shelby", (byte) 30);
        HttpEntity<User> updateHttp = new HttpEntity<>(user3, headers);

        ResponseEntity<String> responseEntityPut = restTemplate.exchange(URL_USERS, HttpMethod.PUT, updateHttp, String.class, params);

        /*--------------------DELETE-----------------*/
        Map<String, Integer> param = new HashMap<>();
        param.put("id", 3);
        HttpEntity<User> deleteHttp = new HttpEntity<>(user3, headers);
        ResponseEntity<String> responseEntityDelete = restTemplate.exchange(URL_USERS_DELETE, HttpMethod.DELETE, deleteHttp, String.class, param);
        System.out.println(responseEntityPost.getBody() + responseEntityPut.getBody()+ responseEntityDelete.getBody());

    }


}
