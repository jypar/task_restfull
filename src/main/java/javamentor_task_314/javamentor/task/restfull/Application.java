package javamentor_task_314.javamentor.task.restfull;

import javamentor_task_314.javamentor.task.restfull.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	private static UserService userService;

	@Autowired
    public Application(UserService userService) {
        this.userService = userService;
    }

    public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

		userService.getUsers();
	}

}
