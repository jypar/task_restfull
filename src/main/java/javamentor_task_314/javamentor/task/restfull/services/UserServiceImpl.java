package javamentor_task_314.javamentor.task.restfull.services;

import javamentor_task_314.javamentor.task.restfull.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void getUsers() {

        userRepository.getUsers();

    }

}
